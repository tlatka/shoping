
#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

class MainList {
public:
    MainList();
    ~MainList() = default;

    ValueTree getMainTree();
    void addList(const std::string& a_Name);

private:
    ValueTree m_MainTree;
};
