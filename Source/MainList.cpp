/*
  ==============================================================================

    Product.cpp
    Created: 14 Jan 2020 9:40:34pm
    Author:  tlatka

  ==============================================================================
*/

#include "MainList.h"

MainList::MainList() {

}


void MainList::addList(const std::string& a_Name) {
    ValueTree newList(Identifier(a_Name.c_str()));
    const int position = -1;
    m_MainTree.addChild(newList, position, nullptr);
}

ValueTree MainList::getMainTree() {
    return m_MainTree;
}

