
#include "ListHeader.h"

ListHeader::ListHeader(ValueTree a_MainTree) :
    m_MainTree(a_MainTree), m_AddList(), m_NewListName("", "...")
{
    m_AddList.setButtonText("add");
    m_AddList.addListener(this);
    m_NewListName.setEditable(true, true, false);

    addAndMakeVisible(m_AddList);
    addAndMakeVisible(m_NewListName);
}

ListHeader::~ListHeader() {
    m_AddList.removeListener(this);
}

void ListHeader::paint(Graphics& g) {
    g.fillAll(Colours::lightgreen);
}

void ListHeader::resized() {

    FlexBox fb;
    fb.flexWrap = FlexBox::Wrap::wrap;
    fb.justifyContent = FlexBox::JustifyContent::center;
    fb.alignContent = FlexBox::AlignContent::center;

    fb.items.add (FlexItem (m_NewListName).withMinWidth(200.0f).withMinHeight(m_ButtonSize).withMargin(FlexItem::Margin(0, m_ButtonSize, 0 ,m_ButtonSize)));
    fb.items.add (FlexItem (m_AddList).withMinWidth(m_ButtonSize).withMinHeight(m_ButtonSize));

    fb.performLayout (getLocalBounds().toFloat());
}

void ListHeader::buttonClicked(Button* a_Button) {
    (void) a_Button;
    printf("Add new list\n");
}
