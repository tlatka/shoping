
#include "ListMain.h"


//==============================================================================
ListMain::ListMain(ValueTree a_MainTree) : m_MainTree(a_MainTree), m_ListHeader(a_MainTree), m_ListList(a_MainTree)
{
	addAndMakeVisible(m_ListHeader);
	addAndMakeVisible(m_ListList);
	setSize(getParentWidth(), getParentHeight());
}

ListMain::~ListMain()
{
}

//==============================================================================
void ListMain::paint (Graphics& g)
{
    //empty
}

void ListMain::resized() {

    FlexBox fb;
    fb.flexDirection = FlexBox::Direction::column;

    FlexItem header(getWidth(), getHeight() *  0.2f, m_ListHeader);
    FlexItem list(getWidth(), getHeight() * 0.8f, m_ListList);

    fb.items.addArray ( { header, list } );
    fb.performLayout (getLocalBounds().toFloat());
}
