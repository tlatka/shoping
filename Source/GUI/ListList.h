
#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

class ListList : public Component {
public:
	ListList(ValueTree a_MainTree);

    void paint (Graphics&) override;
    void resized() override;

private:
    ValueTree m_MainTree;
};
