
#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

class ProductsList : public Component {
public:
	ProductsList() = default;

    void paint (Graphics&) override;
    void resized() override;

private:
};
