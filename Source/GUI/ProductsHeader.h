
#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

class ProductsHeader : public Component {
public:
	ProductsHeader();

    void paint (Graphics&) override;
    void resized() override;

private:
    TextButton m_AddProduct;
    Label m_NewProductName;

    int m_ButtonSize{40};
};
