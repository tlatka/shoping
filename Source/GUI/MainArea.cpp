
#include "MainArea.h"


//==============================================================================
MainArea::MainArea(ValueTree a_MainTree) : m_MainTree(a_MainTree), m_ListMain(m_MainTree)
{
    setSize(600, 400);
	addAndMakeVisible(m_ListMain);
}

//==============================================================================
void MainArea::paint (Graphics& g)
{
    //empty
}

void MainArea::resized() {
    m_ListMain.setSize(getWidth(), getHeight());
}
