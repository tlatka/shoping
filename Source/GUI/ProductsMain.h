
#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "ProductsHeader.h"
#include "ProductsList.h"

class ProductsMain   : public Component
{
public:
    //==============================================================================
    ProductsMain();
    ~ProductsMain();

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;

private:
    ProductsHeader m_ProductsHeader;
    ProductsList m_ProductsList;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ProductsMain)
};
