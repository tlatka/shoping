
#include "ProductsHeader.h"

ProductsHeader::ProductsHeader() : m_AddProduct(), m_NewProductName("", "...") {
    m_AddProduct.setButtonText("add");
    m_NewProductName.setEditable(true, true, false);

    addAndMakeVisible(m_AddProduct);
    addAndMakeVisible(m_NewProductName);
}

void ProductsHeader::paint (Graphics& g) {
    g.fillAll(Colours::red);
}

void ProductsHeader::resized() {
    FlexBox fb;
    fb.flexWrap = FlexBox::Wrap::wrap;
    fb.justifyContent = FlexBox::JustifyContent::center;
    fb.alignContent = FlexBox::AlignContent::center;

    fb.items.add (FlexItem (m_NewProductName).withMinWidth(100.0f).withMinHeight(m_ButtonSize).withMargin(FlexItem::Margin(0, m_ButtonSize, 0 ,m_ButtonSize)));
    fb.items.add (FlexItem (m_AddProduct).withMinWidth(m_ButtonSize).withMinHeight(m_ButtonSize));

    fb.performLayout (getLocalBounds().toFloat());

}
