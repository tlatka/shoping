
#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "ListHeader.h"
#include "ListList.h"

class ListMain   : public Component {
public:
    ListMain(ValueTree a_MainTree);
    ~ListMain();

    void paint (Graphics&) override;
    void resized() override;

private:
    ValueTree m_MainTree;
    ListHeader m_ListHeader;
    ListList m_ListList;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ListMain)
};
