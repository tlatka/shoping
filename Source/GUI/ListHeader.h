
#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

class ListHeader : public Component, Button::Listener {
public:
	ListHeader(ValueTree a_MainTree);
	~ListHeader();
    void paint (Graphics&) override;
    void resized() override;

private:
    ValueTree m_MainTree;

    Colour m_Background;
    TextButton m_AddList;
    Label m_NewListName;

    int m_ButtonSize{40};


    void buttonClicked(Button*) override;
};
