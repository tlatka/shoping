
#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "ListMain.h"

class MainArea   : public Component
{
public:
    //==============================================================================
    MainArea(ValueTree a_MainTree);
    ~MainArea() = default;

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;

private:
    ValueTree m_MainTree;
    ListMain m_ListMain;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainArea)
};
