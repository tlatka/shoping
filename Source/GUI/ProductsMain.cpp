
#include "ProductsMain.h"


//==============================================================================
ProductsMain::ProductsMain() : m_ProductsHeader(), m_ProductsList() {
	addAndMakeVisible(m_ProductsHeader);
	addAndMakeVisible(m_ProductsList);
    setSize (600, 400);
}

ProductsMain::~ProductsMain()
{
}

//==============================================================================
void ProductsMain::paint (Graphics& g)
{
    //empty
}

void ProductsMain::resized()
{
    FlexBox fb;
    fb.flexDirection = FlexBox::Direction::column;

    FlexItem header(getWidth(), getHeight() / 2.0f, m_ProductsHeader);
    FlexItem productList(getWidth(), getHeight() / 2.0f, m_ProductsList);

    fb.items.addArray ( { header, productList } );
    fb.performLayout (getLocalBounds().toFloat());
}
